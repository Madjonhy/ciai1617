package ciai;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ciai.model.Student;

@Controller
@RequestMapping(value = "/students")
public class StudentController {

	static Student[] content =
		{new Student(0,"Ingrid Daubechies",19),
		 new Student(1,"Jacqueline K. Barton",18),
		 new Student(2,"Jane Goodall",20),
		 new Student(3,"Jocelyn Bell Burnell",21),
		 new Student(4,"Johannes Kepler",22),
		 new Student(5,"Lene Vestergaard Hau",17)};
	static ArrayList<Student> students = new ArrayList<>(Arrays.asList(content));

	@RequestMapping(value = "")
	public @ResponseBody List<Student> getStudents() {
		return students;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody Student getStudent(@PathVariable int id) {
		return students.get(id);
	}

	@RequestMapping(value= "", method=RequestMethod.POST)
	public @ResponseBody int addStudent(@RequestBody Student student) {
		student.setId(students.size());
		students.add(student);
		return student.getId();
	}
}
